# Setup

A `stable.user.js`-ben a következő sorokat módosítsd a helyi szerver használatához:

## 0

Tampermonkey-ban tiltsd le az eddig használt scriptet, hogy ne akadjanak össze

## 1

```javascript
const serverAdress = 'https://qmining.frylabs.net/'
// const serverAdress = 'http://localhost:8080/'
const apiAdress = 'https://api.frylabs.net/'
// const apiAdress = 'http://localhost:8080/'
```

Itt a `localhost`-os címeket uncommenteld, a frylabs-osakat pedig commenteld ki!

## 2

Nyisd meg a tampermonkey bővítményt, majd hozz létre egy új scriptet

A megnyíló ablakban tölrölj mindent a szerkesztőben, és a `devel/testWrapper.js` fájl tartalmát
másold be úgy ahogy van.

## 3

A bemásolt tartalomban keresd meg a következő részt:
```
// @require      file:///{ELÉRÉSI ÚT IDE}
```
Ide írd be a `stable.user.js` elérési útvonalát. Ezt legkönyebben úgy teheted meg, hogy fájl
böngészőből behúzod az URL bárba a `stable.user.js`-t magát. Ekkor felugorhat a tampermonkey hogy
telepíteni akarja, de azt zárd be és egy üres ablakban ott kellene lennie az URL bárban a teljes
elérési útnak. Ezt úgy hogy a `file:///` legyen az eleje másold be a a `@require` tag-hez

## 4

Ha minden jól ment, akkor a moddle oldalán már a helyi változatnak kellene futnia, és minden
`stable.user.js` módosítás azonnal életbe lép a böngészőben való frissítés után.
