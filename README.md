# Moodle/KMOOC teszt megoldó userscript

## Telepítés
1. [Greasyfork leírása userscript futtató addon telepítésére](https://greasyfork.org/en/help/installing-user-scripts)
2. [Telepítés hivatalos weboldalról](http://qmining.frylabs.net/install)

Első használatkor és frissítéseknél engedélyezni kell a webszerver elérését, de erre külön figyelmeztet. Ezután már használható is.


# Működés

A userscript a teszt oldal DOM-jében megkeresi a kérdést/kérdéseket, és a hozzá tartozó egyéb adatokat (pl.: kép), és azokat a webszervernek elküldi. A szerver válaszában szerepel az összes lehetséges helyes válasz, és hogy az mekkora százalékkal a tényleges helyes válasz.

Teszt eredmény oldalon végigjárja a kérdéseket, és a helyes megoldást szintén elküldi a szervernek, ami eldönti hogy ilyen kérdés van-e már, és ha nincs akkor hozzáadja az adatbázishoz.

![alt text](http://qmining.frylabs.net/img/screens/2.png)

# Egyéb
[Szerver repository](https://gitlab.com/MrFry/question-node-server)

Jelenleg sok optimalizálatlan rész található benne, cél ezek kijavítása, szépítése

# Licensz:
GPLv3
